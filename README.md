# FastAPI Demo

## How to initialize
```sh
pip install fastapi uvicorn
```

## How to Run
```sh
uvicorn main:app --reload
```

from fastapi import FastAPI

import random

app = FastAPI()

@app.get("/autoloan")
def score_autoloan(first_name:str, last_name:str, age:int):
    
    # Demo ML Model
    score = random.randint(0, 99)
    
    return {
        "result": score
    }
